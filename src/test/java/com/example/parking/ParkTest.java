package com.example.parking;

import com.example.parking.boy.SmartParkingBoy;
import com.example.parking.boy.SuperParkingBoy;
import com.example.parking.car.Car;
import com.example.parking.car.CarFactory;
import com.example.parking.lot.ParkingLotGroup;
import com.example.parking.lot.SimpleParkingLot;
import com.example.parking.manager.SimpleParkingManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;

public class ParkTest {


    @Test
    void carShouldBeParking() throws ParkingException {
        SimpleParkingLot lot = new SimpleParkingLot(5);
        Car car = CarFactory.getInstance().buildCar("AABBCC");
        Assertions.assertDoesNotThrow(() -> lot.parkCar(car), "car should be parking without exception.");
        Assertions.assertTrue(lot.searchCar(car), "car should in the parking.");
    }

    @Test
    void carShouldBeRetrieving() throws ParkingException {
        SimpleParkingLot lot = new SimpleParkingLot(5);
        Car car = CarFactory.getInstance().buildCar("AABBCC");
        Token token = lot.parkCar(car);
        Assertions.assertTrue(lot.searchCar(car), "car should in the parking.");
        Assertions.assertTrue(lot.retrieveCar(token).map(car::equals).orElse(false), "It should be same car.");
    }

    @Test
    void carShouldNotBeParking() throws ParkingException {
        SimpleParkingLot lot = new SimpleParkingLot(0);
        Car car = CarFactory.getInstance().buildCar("AABBCC");
        Assertions.assertThrows(ParkingException.class, () -> lot.parkCar(car));
    }

    @Test
    void wrongTokenShouldNotRetrieveCar() throws ParkingException {
        SimpleParkingLot lot = new SimpleParkingLot(5);
        Car car = CarFactory.getInstance().buildCar("AABBCC");
        lot.parkCar(car);
        Token wrongToken = new Token("123456", null);
        Assertions.assertFalse(lot.retrieveCar(wrongToken).isPresent());
    }

    @Test
    void cannotBuildACarWithoutPlateNumber() {
        Assertions.assertThrows(ParkingException.class, () -> CarFactory.getInstance().buildCar(""));
    }


    @Test
    void superParkingBoyShouldParkCar() throws ParkingException {
        SimpleParkingLot lot = new SimpleParkingLot(5);
        Car car = CarFactory.getInstance().buildCar("AABBCC");
        Token token = new SuperParkingBoy(new ParkingLotGroup(Collections.singletonList(lot))).park(car);
        Assertions.assertNotNull(token);
    }

    @Test
    void smartParkingBoyShouldParkCar() throws ParkingException {
        SimpleParkingLot lot = new SimpleParkingLot(5);
        Car car = CarFactory.getInstance().buildCar("AABBCC");
        Token token = new SmartParkingBoy(new ParkingLotGroup(Collections.singletonList(lot))).park(car);
        Assertions.assertNotNull(token);
    }

    @Test
    void parkingManagerShouldAssignBoyParkCar() throws ParkingException {
        SimpleParkingLot lot = new SimpleParkingLot(5);
        SimpleParkingManager manager = new SimpleParkingManager(Collections.singletonList(lot));
    }

}