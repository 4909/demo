package com.example.length;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class LengthTest {

    @Test
    void testLengthABiggerThanB(){

        Length l1 = LengthFactory.getInstance().retrieveLength(LengthUnit.METER, 1);
        Length l2 = LengthFactory.getInstance().retrieveLength(LengthUnit.C_METER, 99);

        Assert.isTrue(l1.compareTo(l2) > 0, "l1 should bigger than l2.");
    }

    @Test
    void testLengthASmallerThanB(){

        Length l1 = LengthFactory.getInstance().retrieveLength(LengthUnit.METER, 1);
        Length l2 = LengthFactory.getInstance().retrieveLength(LengthUnit.C_METER, 120);

        Assert.isTrue(l1.compareTo(l2) < 0, "l1 should smaller than l2.");
    }

    @Test
    void testLAEqualToB1(){

        Length l1 = LengthFactory.getInstance().retrieveLength(LengthUnit.METER, 1000);
        Length l2 = LengthFactory.getInstance().retrieveLength(LengthUnit.K_METER, 1);

        Assert.isTrue(l1.compareTo(l2) == 0, "l1 should equal to l2.");
    }

    @Test
    void testLAEqualToB2(){

        Length l1 = LengthFactory.getInstance().retrieveLength(LengthUnit.METER, 1000);
        Length l2 = LengthFactory.getInstance().retrieveLength(LengthUnit.K_METER, 1);

        Assert.isTrue(l1.equals(l2), "l1 should equal to l2.");
    }

    @Test
    void lengthPrint(){

        Length l1 = LengthFactory.getInstance().retrieveLength(LengthUnit.METER, 1);

        try {
            new SimpleLengthPrintBridge(l1).print();
        }
        catch (Exception e){
            Assertions.fail("length should print without exception.");
        }
    }

}
