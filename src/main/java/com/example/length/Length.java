package com.example.length;

import java.math.BigDecimal;
import java.util.Objects;

public class Length implements Comparable<Length> {

    private BigDecimal value;
    private LengthUnit unit;


    public Length(LengthUnit unit, BigDecimal value){
        this.value = value;
        this.unit = unit;
    }

    @Override
    public int compareTo(Length l) {
        return LengthUnit.parseUnit(this, l.unit).getValue().compareTo(l.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return LengthUnit.parseUnit((Length) o, unit).value.compareTo(this.value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public LengthUnit getUnit() {
        return unit;
    }

    public void setUnit(LengthUnit unit) {
        this.unit = unit;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }


}
