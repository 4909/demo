package com.example.length;

import com.example.length.printer.SimpleLengthPrinter;

public class SimpleLengthPrintBridge extends LengthPrintBridge<SimpleLengthPrinter> {

    public SimpleLengthPrintBridge(Length length) {
        super(length);
    }

}
