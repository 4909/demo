package com.example.length.printer;

import com.example.length.Length;

public interface LengthPrinter {

    void print(Length length);

}
