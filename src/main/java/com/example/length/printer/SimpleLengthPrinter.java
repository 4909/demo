package com.example.length.printer;

import com.example.length.Length;

public class SimpleLengthPrinter implements LengthPrinter {
    public void print(Length length){
        System.out.print(length.getValue() + ":");
        System.out.println(length.getUnit().name());
    }
}
