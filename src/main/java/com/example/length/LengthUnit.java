package com.example.length;

public enum LengthUnit {


    K_METER(7),
    METER(10),
    C_METER(12),
    M_METER(13);

    final private int value;

    LengthUnit(int value) {
        this.value = value;
    }

    public static Length parseUnit(Length length, LengthUnit unit) {
        return LengthFactory.getInstance().retrieveLength(
                unit,
                length.getValue().scaleByPowerOfTen(unit.value - length.getUnit().value)
        );
    }
}
