package com.example.length;

import java.math.BigDecimal;

public class LengthFactory {

    private LengthFactory(){

    }

    public static LengthFactory getInstance(){
        if(lengthFactory == null) {
            synchronized(LengthFactory.class) {
                if(lengthFactory == null) {
                    lengthFactory = new LengthFactory();
                }
            }
        }
        return lengthFactory;
    }

    private static volatile LengthFactory lengthFactory;

    public Length retrieveLength(LengthUnit unit, BigDecimal value){
        return new Length(unit, value);
    }

    public Length retrieveLength(LengthUnit unit, double value){
        return new Length(unit, BigDecimal.valueOf(value));
    }


}
