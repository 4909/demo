package com.example.parking.boy;

import com.example.parking.lot.ParkingLotGroup;
import com.example.parking.strategy.VacancyRateParkingLotSelectStrategy;

public class SuperParkingBoy extends ParkingBoy<VacancyRateParkingLotSelectStrategy> {
    public SuperParkingBoy(ParkingLotGroup parkingLotGroup) {
        super(parkingLotGroup);
    }
}
