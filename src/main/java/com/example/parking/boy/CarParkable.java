package com.example.parking.boy;

import com.example.parking.ParkingException;
import com.example.parking.Token;
import com.example.parking.car.Car;
import com.example.parking.director.Reportable;

import java.util.Optional;

public interface CarParkable extends Reportable {

    Token park(Car car) throws ParkingException;
    Optional<Car> retrieveCar(Token token) throws ParkingException;
}
