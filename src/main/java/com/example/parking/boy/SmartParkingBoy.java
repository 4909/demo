package com.example.parking.boy;

import com.example.parking.lot.ParkingLotGroup;
import com.example.parking.strategy.VacancyParkingLotSelectStrategy;

public class SmartParkingBoy extends ParkingBoy<VacancyParkingLotSelectStrategy> {
    public SmartParkingBoy(ParkingLotGroup parkingLotGroup) {
        super(parkingLotGroup);
    }
}
