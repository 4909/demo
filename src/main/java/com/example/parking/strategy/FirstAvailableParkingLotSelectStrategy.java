package com.example.parking.strategy;

import com.example.parking.ParkingException;
import com.example.parking.lot.ParkingLot;

import java.util.List;

public class FirstAvailableParkingLotSelectStrategy implements ParkingLotSelectStrategy {
    @Override
    public ParkingLot chooseParkingLot(List<? extends ParkingLot> parkingLots) throws ParkingException {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.availablePlace() > 0)
                .findFirst()
                .orElseThrow(ParkingException::new);
    }
}
