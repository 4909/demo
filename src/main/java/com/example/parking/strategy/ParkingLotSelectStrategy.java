package com.example.parking.strategy;

import com.example.parking.ParkingException;
import com.example.parking.lot.ParkingLot;

import java.util.List;

public interface ParkingLotSelectStrategy {

    ParkingLot chooseParkingLot(List<? extends ParkingLot> parkingLots) throws ParkingException;

}
