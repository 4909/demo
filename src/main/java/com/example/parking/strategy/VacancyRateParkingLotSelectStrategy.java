package com.example.parking.strategy;

import com.example.parking.ParkingException;
import com.example.parking.lot.ParkingLot;

import java.util.Comparator;
import java.util.List;

public class VacancyRateParkingLotSelectStrategy implements ParkingLotSelectStrategy {
    @Override
    public ParkingLot chooseParkingLot(List<? extends ParkingLot> parkingLots) throws ParkingException {

        parkingLots.sort(Comparator.comparingDouble(ParkingLot::getVacancyRate));
        if(parkingLots.get(0).availablePlace() > 0) {
            return parkingLots.get(0);
        }

        throw new ParkingException();
    }
}
