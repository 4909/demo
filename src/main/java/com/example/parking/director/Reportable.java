package com.example.parking.director;

public interface Reportable {
    void showReport();
}
