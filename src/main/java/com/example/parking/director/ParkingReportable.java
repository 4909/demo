package com.example.parking.director;

import java.util.List;

public class ParkingReportable implements Reportable {

    private List<Reportable> parkingManagerList;

    @Override
    public void showReport() {
        parkingManagerList.forEach(Reportable::showReport);
    }
}
