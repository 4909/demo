package com.example.parking.car;

import com.example.parking.ParkingException;
import org.springframework.util.StringUtils;

public class CarFactory {

    private static volatile CarFactory carFactory;

    private CarFactory(){

    }

    public static CarFactory getInstance(){
        if(carFactory == null) {
            synchronized(CarFactory.class) {
                if(carFactory == null) {
                    carFactory = new CarFactory();
                }
            }
        }
        return carFactory;
    }

    public Car buildCar(String carPlateNumber) throws ParkingException {
        if(!StringUtils.hasText(carPlateNumber)) {
            throw new ParkingException();
        }
        return new Car(carPlateNumber);
    }

}
