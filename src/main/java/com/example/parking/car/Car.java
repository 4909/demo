package com.example.parking.car;

public class Car {

    private final String plateNumber;

    public Car(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getPlateNumber(){
        return this.plateNumber;
    }

    @Override
    public String toString() {
        return this.plateNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return this.plateNumber.equals(car.getPlateNumber());
    }
}
