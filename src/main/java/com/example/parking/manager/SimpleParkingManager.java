package com.example.parking.manager;

import com.example.parking.ParkingException;
import com.example.parking.Token;
import com.example.parking.boy.CarParkable;
import com.example.parking.car.Car;
import com.example.parking.lot.ParkingLot;
import com.example.parking.lot.ParkingLotGroup;
import com.example.parking.strategy.FirstAvailableParkingLotSelectStrategy;
import com.example.parking.strategy.ParkingLotSelectStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class SimpleParkingManager extends ParkingManager implements CarParkable {

    private ParkingLotSelectStrategy parkingLotSelectStrategy;

    private ParkingLotGroup privateParkingGroup;

    public SimpleParkingManager(List<ParkingLot> parkingLots) {
        super(new ParkingLotGroup(parkingLots));
        privateParkingGroup = new ParkingLotGroup(parkingLots); //TODO: assign parking group.
        parkingLotSelectStrategy = new FirstAvailableParkingLotSelectStrategy();
    }

    public SimpleParkingManager(List<ParkingLot> parkingLots, List<CarParkable> carParkables) {
        super(new ParkingLotGroup(parkingLots), carParkables);
        privateParkingGroup = new ParkingLotGroup(parkingLots); //TODO: assign parking group.
        parkingLotSelectStrategy = new FirstAvailableParkingLotSelectStrategy();
    }

    public void setParkingLotSelectStrategy(ParkingLotSelectStrategy parkingLotSelectStrategy) {
        this.parkingLotSelectStrategy = parkingLotSelectStrategy;
    }

    @Override
    public Token park(Car car) throws ParkingException {
        ParkingLot parkingLot = parkingLotSelectStrategy.chooseParkingLot(super.parkingLotGroup.getParkingLots());
        return super.parkingLotGroup.park(parkingLot, car);
    }

    @Override
    public Optional<Car> retrieveCar(Token token) throws ParkingException {
        return super.parkingLotGroup.retrieveCar(token);
    }

    @Override
    public void showReport() {
        System.out.println("M "
                + this.parkingLotGroup.retrieveParkingLotCount()
                + " "
                + this.parkingLotGroup.retrieveParkingLotCarCount()
        );
        System.out.println("P "
                + this.privateParkingGroup.retrieveParkingLotCount()
                + " "
                + this.privateParkingGroup.retrieveParkingLotCarCount()
        );
        super.showReport();
    }

    @Override
    protected CarParkable retrieveCarParker(Car car) {
        return super.carParkables.get(new Random().nextInt(super.carParkables.size()));

    }
}
