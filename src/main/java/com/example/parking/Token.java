package com.example.parking;

import com.example.parking.lot.ParkingLot;

public class Token {

    private final String carPlateNumber;
    private final ParkingLot parkingLot;

    public Token(String carPlateNumber, ParkingLot parkingLot){
        this.carPlateNumber = carPlateNumber;
        this.parkingLot = parkingLot;
    }
    @Override
    public String toString() {
        return carPlateNumber;
    }

    public ParkingLot getParkingLot() {
        return parkingLot;
    }
}
