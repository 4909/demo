package com.example.parking.lot;

import com.example.parking.car.Car;
import com.example.parking.ParkingException;
import com.example.parking.Token;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class SimpleParkingLot implements ParkingLot {

    private final Map<Token, Car> park;
    private final int maxArea;

    public SimpleParkingLot(int maxArea) {
        this.park = new HashMap<>();
        this.maxArea = maxArea;
    }

    @Override
    public Optional<Car> retrieveCar(Token token) {
        Car car = park.get(token);
        return Optional.ofNullable(park.remove(token));
    }

    @Override
    public Token parkCar(Car car) throws ParkingException {
        if(park.keySet().size() == maxArea){
            throw new ParkingException();
        }
        Token token = new Token(car.getPlateNumber(), this);
        park.put(token, car);
        return token;
    }

    @Override
    public boolean searchCar(Car car) {
        return park.containsValue(car);
    }

    @Override
    public int getCarCount() {
        return park.values().size();
    }

    @Override
    public int availablePlace() {
        return this.maxArea - park.values().size();
    }

    @Override
    public double getVacancyRate() {
        return ((double) park.values().size()) / this.maxArea;
    }

    @Override
    public void showReport() {
        System.out.println("P "
                + this.park.values().size()
                + " "
                + this.maxArea
        );
    }
}
