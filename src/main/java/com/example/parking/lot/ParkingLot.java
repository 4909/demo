package com.example.parking.lot;

import com.example.parking.car.Car;
import com.example.parking.ParkingException;
import com.example.parking.Token;
import com.example.parking.director.Reportable;

import java.util.Optional;

public interface ParkingLot extends Reportable {

    Optional<Car> retrieveCar(Token token);

    Token parkCar(Car car) throws ParkingException;

    boolean searchCar(Car car);

    int getCarCount();
    
    int availablePlace();

    double getVacancyRate();


}
